# Farmcrowdy Food

### Stack used

- EJS
- SCSS

### Usage

- Run `npm install` or `yarn install`
- Run `npm run build` or `yarn build` to build latest changes
- Copy the build folder
