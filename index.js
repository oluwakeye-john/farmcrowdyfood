const fs = require("fs");
const ejs = require("ejs");
const glob = require("glob");

glob(
  "**/*.ejs",
  {
    ignore: ["src/views/partials/*"],
  },
  (err, files) => {
    files.forEach((file) => {
      ejs.renderFile(file, function (error, str) {
        if (error) {
          throw Error(error);
        }
        const arr = file.split("/");
        const length = arr.length;
        const name = arr[length - 1];

        const newName = name.replace("ejs", "html");
        const newPath = `./public/${newName}`;

        if (error) {
          return console.log(err);
        }
        fs.writeFile(newPath, str, (fserror) => {
          if (fserror) return console.log(fserror);
          console.log(`updated ${newPath}`);
        });
      });
    });
  }
);
