(function () {
  let quantity = 1;

  let plusQuantityButton = document.querySelector("#plus-quantity");
  let minusQuantityButton = document.querySelector("#minus-quantity");
  let quantityText = document.querySelector("#quantity-text");

  const setQuantity = (num) => {
    quantityText.innerText = num;
  };

  plusQuantityButton.addEventListener("click", () => {
    quantity += 1;
    setQuantity(quantity);
  });

  minusQuantityButton.addEventListener("click", () => {
    if (quantity > 1) {
      quantity -= 1;
      setQuantity(quantity);
    }
  });
})();
