(function () {
  $(".main-carousel ").slick({
    prevArrow: $("#prev"),
    nextArrow: $("#next"),
    infinite: false,
    autoplay: true,
  });

  $(".product-carousel").slick({
    infinite: false,
    slidesToShow: 4,
    // arrows: false,
    // centerMode: true,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          // dots: true,
          arrows: false,
        },
      },
      {
        breakpoint: 400,
        settings: {
          arrows: false,
          slidesToShow: 2,
          // dots: true,
        },
      },
    ],
  });
})();
