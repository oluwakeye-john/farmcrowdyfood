(function () {
  const navbarNav = document.querySelector("#navbar__nav");
  const navbarToggler = document.querySelector("#navbar__toggler");

  let navbarOpened = false;
  navbarToggler.addEventListener("click", () => {
    console.log("clicked");

    navbarOpened = !navbarOpened;
    if (navbarOpened) {
      navbarNav.classList.add("show-navbar");
    } else {
      navbarNav.classList.remove("show-navbar");
    }
  });

  const copyrightYear = document.querySelector("#copyright-year");

  const year = new Date().getFullYear();

  copyrightYear.innerText = year;
})();
